//
//
//
export class EmptyStringIdentifierError extends Error {
    public static isEmptyStringIdentifierError(value: unknown): value is EmptyStringIdentifierError {
        return value instanceof EmptyStringIdentifierError;
    }

    public static throw(): never {
        throw new EmptyStringIdentifierError();
    }

    public constructor() {
        super('Empty string can not be used as an identifier.');
    }
}
