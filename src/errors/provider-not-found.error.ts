import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
//
//
//
export class ProviderNotFoundError extends Error {
    public static isProviderNotFoundError(value: unknown): value is ProviderNotFoundError {
        return value instanceof ProviderNotFoundError;
    }

    public static throw(identifier: Identifier | InjectionToken<unknown>): never {
        throw new ProviderNotFoundError(identifier);
    }

    public constructor(
        public readonly identifier: Identifier | InjectionToken<unknown>
    ) {
        super(`Provider not found for identifier. identifier: '${Identifier.stringify(identifier)}'`);
    }
}
