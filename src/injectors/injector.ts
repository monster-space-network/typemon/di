import { Type, Newable } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ParameterMetadata, MetadataKey } from '../metadata';
import { SyncProvider, ProviderMapper, ClassProvider, ConstructorProvider, FactoryProvider, ExistingProvider, ValueProvider } from '../providers';
import { ProviderNotFoundError } from '../errors';
import { ResolutionStrategy } from './resolution-strategy';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/injector
 */
export class Injector {
    public static isInjector(value: unknown): value is Injector {
        return value instanceof Injector;
    }

    public static create(providers: ReadonlyArray<SyncProvider>, options: Injector.Options = {}): Injector {
        const injector: Injector = new Injector(providers, options.parent);

        if (options.resolutionStrategy === ResolutionStrategy.Lazy) {
            return injector;
        }

        for (const provider of providers) {
            injector.get(provider.identifier);
        }

        return injector;
    }

    private readonly providers: Map<Identifier, SyncProvider | ReadonlyArray<SyncProvider>>;
    private readonly dependencies: Map<Identifier, unknown>;
    private readonly parent: Injector | null;

    public constructor(providers: ReadonlyArray<SyncProvider>, parent?: Injector) {
        this.providers = ProviderMapper.map(providers);
        this.dependencies = new Map();
        this.parent = parent || null;
    }

    private getDependencyByMetadata(metadata: ParameterMetadata): unknown {
        try {
            const identifier: Identifier = metadata.identifier as Identifier;
            const dependency: unknown = metadata.flag & ParameterMetadata.Flag.Self
                ? this.dependencies.has(identifier)
                    ? this.dependencies.get(identifier)
                    : this.providers.has(identifier)
                        ? this.resolve(identifier)
                        : ProviderNotFoundError.throw(identifier)
                : metadata.flag & ParameterMetadata.Flag.SkipSelf
                    ? Type.isNull(this.parent)
                        ? ProviderNotFoundError.throw(identifier)
                        : this.parent.get(identifier)
                    : this.get(identifier);

            return dependency;
        }
        catch (error) {
            if (ProviderNotFoundError.isProviderNotFoundError(error) && metadata.flag & ParameterMetadata.Flag.Optional) {
                return undefined;
            }

            throw error;
        }
    }
    private getDependenciesByConstructor(target: Newable<object>): ReadonlyArray<unknown> {
        const metadatas: ReadonlyArray<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target);

        return metadatas.map((metadata: ParameterMetadata): unknown => this.getDependencyByMetadata(metadata));
    }
    private getDependenciesByIdentifiers(identifiers: ReadonlyArray<Identifier>): ReadonlyArray<unknown> {
        return identifiers.map((identifier: Identifier): unknown => this.get(identifier));
    }

    private resolveProvider(provider: SyncProvider): unknown {
        if (ValueProvider.isValueProvider(provider)) {
            return provider.value;
        }

        if (ClassProvider.isClassProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = this.getDependenciesByConstructor(provider.target);

            return new provider.target(...dependencies);
        }

        if (ConstructorProvider.isConstructorProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = this.getDependenciesByIdentifiers(provider.dependencies);

            return new provider.target(...dependencies);
        }

        if (ExistingProvider.isExistingProvider(provider)) {
            return this.get(provider.target);
        }

        if (FactoryProvider.isFactoryProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = this.getDependenciesByIdentifiers(provider.dependencies);

            return provider.callback(...dependencies);
        }
    }
    private resolveProviders(providers: SyncProvider | Array<SyncProvider>): unknown {
        return Type.isArray(providers)
            ? providers.map((provider: SyncProvider): unknown => this.resolveProvider(provider))
            : this.resolveProvider(providers);
    }
    private resolve(identifier: Identifier): unknown {
        const providers: SyncProvider | ReadonlyArray<SyncProvider> | undefined = this.providers.get(identifier);

        return Type.isUndefined(providers)
            ? Type.isNull(this.parent)
                ? ProviderNotFoundError.throw(identifier)
                : this.parent.get(identifier)
            : this.dependencies.set(identifier, this.resolveProviders(providers as Array<SyncProvider>)).get(identifier);
    }

    public get(identifier: Identifier | InjectionToken<unknown>): any {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        return this.dependencies.has(identifier)
            ? this.dependencies.get(identifier)
            : this.resolve(identifier);
    }
}
export namespace Injector {
    export interface Options {
        /**
         * @see https://gitlab.com/monster-space-network/typemon/di/wikis/hierarchical-injector
         */
        readonly parent?: Injector;

        /**
         * @see https://gitlab.com/monster-space-network/typemon/di/wikis/resolution-strategy
         */
        readonly resolutionStrategy?: ResolutionStrategy;
    }
}
