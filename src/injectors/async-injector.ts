import { Type, Newable } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { MetadataKey, ParameterMetadata } from '../metadata';
import {
    Provider, ClassProvider, ConstructorProvider, FactoryProvider, ExistingProvider, ValueProvider, AsyncFactoryProvider,
    ProviderMapper
} from '../providers';
import { ProviderNotFoundError } from '../errors';
import { ResolutionStrategy } from './resolution-strategy';
import { Injector } from './injector';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/asynchronous-injector
 */
export class AsyncInjector {
    public static isAsyncInjector(value: unknown): value is AsyncInjector {
        return value instanceof AsyncInjector;
    }

    public static create(providers: ReadonlyArray<Provider>, options?: AsyncInjector.Options & { readonly resolutionStrategy: ResolutionStrategy.Lazy }): AsyncInjector;
    public static create(providers: ReadonlyArray<Provider>, options?: AsyncInjector.Options & { readonly resolutionStrategy?: ResolutionStrategy.Default }): Promise<AsyncInjector>;
    public static create(providers: ReadonlyArray<Provider>, options: AsyncInjector.Options = {}): AsyncInjector | Promise<AsyncInjector> {
        const injector: AsyncInjector = new AsyncInjector(providers, options.parent);

        if (options.resolutionStrategy === ResolutionStrategy.Lazy) {
            return injector;
        }

        return providers.reduce((promise: Promise<void>, provider: Provider): Promise<void> => {
            return promise.then((): Promise<void> => injector.get(provider.identifier));
        }, Promise.resolve()).then((): AsyncInjector => injector);
    }

    private readonly providers: Map<Identifier, Provider | ReadonlyArray<Provider>>;
    private readonly dependencies: Map<Identifier, unknown>;
    private readonly parent: AsyncInjector | Injector | null;

    public constructor(providers: ReadonlyArray<Provider>, parent?: Injector | AsyncInjector) {
        this.providers = ProviderMapper.map(providers);
        this.dependencies = new Map();
        this.parent = parent || null;
    }

    private async getDependencyByMetadata(metadata: ParameterMetadata): Promise<unknown> {
        try {
            const identifier: Identifier = metadata.identifier as Identifier;
            const promise: Promise<unknown> = metadata.flag & ParameterMetadata.Flag.Self
                ? this.dependencies.has(identifier)
                    ? this.dependencies.get(identifier)
                    : this.providers.has(identifier)
                        ? this.resolve(identifier)
                        : ProviderNotFoundError.throw(identifier)
                : metadata.flag & ParameterMetadata.Flag.SkipSelf
                    ? Type.isNull(this.parent)
                        ? ProviderNotFoundError.throw(identifier)
                        : this.parent.get(identifier)
                    : this.get(identifier);
            const dependency: unknown = await promise;

            return dependency;
        }
        catch (error) {
            if (ProviderNotFoundError.isProviderNotFoundError(error) && metadata.flag & ParameterMetadata.Flag.Optional) {
                return undefined;
            }

            throw error;
        }
    }
    private async getDependenciesByConstructor(target: Newable<object>): Promise<ReadonlyArray<unknown>> {
        const metadatas: ReadonlyArray<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target);
        const dependencies: Array<unknown> = [];

        for (const metadata of metadatas) {
            const dependency: unknown = await this.getDependencyByMetadata(metadata);

            dependencies.push(dependency);
        }

        return dependencies;
    }
    private async getDependenciesByIdentifiers(identifiers: ReadonlyArray<Identifier>): Promise<ReadonlyArray<unknown>> {
        const dependencies: Array<unknown> = [];

        for (const identifier of identifiers) {
            const dependency: unknown = await this.get(identifier);

            dependencies.push(dependency);
        }

        return dependencies;
    }

    private async resolveProvider(provider: Provider): Promise<unknown> {
        if (ValueProvider.isValueProvider(provider)) {
            return provider.value;
        }

        if (ClassProvider.isClassProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = await this.getDependenciesByConstructor(provider.target);

            return new provider.target(...dependencies);
        }

        if (ConstructorProvider.isConstructorProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = await this.getDependenciesByIdentifiers(provider.dependencies);

            return new provider.target(...dependencies);
        }

        if (ExistingProvider.isExistingProvider(provider)) {
            return this.get(provider.target);
        }

        if (FactoryProvider.isFactoryProvider(provider) || AsyncFactoryProvider.isAsyncFactoryProvider(provider)) {
            const dependencies: ReadonlyArray<unknown> = await this.getDependenciesByIdentifiers(provider.dependencies);

            return provider.callback(...dependencies);
        }
    }
    private async resolveProviders(providers: Provider | Array<Provider>): Promise<unknown> {
        if (Type.isNotArray(providers)) {
            return this.resolveProvider(providers);
        }

        const dependencies: Array<unknown> = [];

        for (const provider of providers) {
            const dependency: unknown = await this.resolveProvider(provider);

            dependencies.push(dependency);
        }

        return dependencies;
    }
    private async resolve(identifier: Identifier): Promise<unknown> {
        const providers: Provider | ReadonlyArray<Provider> | undefined = this.providers.get(identifier);

        if (Type.isUndefined(providers)) {
            return Type.isNull(this.parent)
                ? ProviderNotFoundError.throw(identifier)
                : Injector.isInjector(this.parent)
                    ? this.parent.get(identifier)
                    : this.parent.get(identifier);
        }

        const dependency: unknown = await this.resolveProviders(providers as Array<Provider>);

        this.dependencies.set(identifier, dependency);

        return dependency;
    }

    public async get(identifier: Identifier | InjectionToken<unknown>): Promise<any> {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        return this.dependencies.has(identifier)
            ? this.dependencies.get(identifier)
            : this.resolve(identifier);
    }
}
export namespace AsyncInjector {
    export interface Options {
        /**
         * @see https://gitlab.com/monster-space-network/typemon/di/wikis/hierarchical-injector
         */
        readonly parent?: AsyncInjector | Injector;

        /**
         * @see https://gitlab.com/monster-space-network/typemon/di/wikis/resolution-strategy
         */
        readonly resolutionStrategy?: ResolutionStrategy;
    }
}
