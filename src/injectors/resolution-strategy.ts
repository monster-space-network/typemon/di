//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/resolution-strategy
 */
export enum ResolutionStrategy {
    Default = 'defalut',
    Lazy = 'lazy'
}
