import { Newable } from '@typemon/types';
//
import { ValueProvider } from './value.provider';
import { ClassProvider } from './class.provider';
import { ConstructorProvider } from './constructor.provider';
import { ExistingProvider } from './existing.provider';
import { FactoryProvider } from './factory.provider';
import { AsyncFactoryProvider } from './async-factory.provider';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers
 */
export type SyncProvider
    = ValueProvider
    | ClassProvider
    | ConstructorProvider
    | ExistingProvider
    | FactoryProvider;

/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers
 */
export type AsyncProvider = AsyncFactoryProvider;

/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers
 */
export type Provider = SyncProvider | AsyncProvider;
export namespace Provider {
    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#default-provider
     */
    export function use(target: Newable<object>): ClassProvider {
        return new ClassProvider({
            identifier: target,
            target
        });
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#value-provider
     */
    export function useValue<Type>(data: ValueProvider.Data<Type>): ValueProvider {
        return new ValueProvider(data);
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#class-provider
     */
    export function useClass<Type extends object>(data: ClassProvider.Data<Type>): ClassProvider {
        return new ClassProvider(data);
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#constructor-provider
     */
    export function useConstructor<Type extends object>(data: ConstructorProvider.Data<Type>): ConstructorProvider {
        return new ConstructorProvider(data);
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#existing-provider
     */
    export function useExisting<Type>(data: ExistingProvider.Data<Type>): ExistingProvider {
        return new ExistingProvider(data);
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#factory-provider
     */
    export function useFactory<Type>(data: FactoryProvider.Data<Type>): FactoryProvider {
        return new FactoryProvider(data);
    }

    /**
     * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#async-factory-provider
     */
    export function useAsyncFactory<Type>(data: AsyncFactoryProvider.Data<Type>): AsyncFactoryProvider {
        return new AsyncFactoryProvider(data);
    }
}
