//
//
//
export enum ProviderType {
    Value = 'value',
    Class = 'class',
    Constructor = 'constructor',
    Existing = 'existing',
    Factory = 'factory',
    AsyncFactory = 'async-factory'
}
