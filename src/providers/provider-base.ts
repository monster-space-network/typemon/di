import { Identifier, Identifiable } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ProviderType } from './provider-type';
//
//
//
export class ProviderBase implements Identifiable, ProviderBase.Data<unknown> {
    public readonly identifier: Identifier;
    public readonly multiple: boolean;

    protected constructor(
        public readonly type: ProviderType,
        { identifier, multiple }: ProviderBase.Data<unknown>
    ) {
        this.identifier = InjectionToken.isInjectionToken(identifier) ? identifier.identifier : identifier;
        this.multiple = multiple || false;
    }
}
export namespace ProviderBase {
    export interface Data<Type> {
        readonly identifier: Identifier | InjectionToken<Type>;
        readonly multiple?: boolean;
    }
}
