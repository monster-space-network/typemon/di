import { Type } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { MetadataKey } from '../metadata';
import { SyncProvider, AsyncProvider } from './provider';
import { ClassProvider } from './class.provider';
import { ConstructorProvider } from './constructor.provider';
//
//
//
export namespace ProviderMapper {
    export function map<Provider extends SyncProvider | AsyncProvider>(providers: ReadonlyArray<Provider>): Map<Identifier, Provider | ReadonlyArray<Provider>> {
        const map: Map<Identifier, Provider | Array<Provider>> = new Map();

        for (const provider of providers) {
            if ((ClassProvider.isClassProvider(provider) || ConstructorProvider.isConstructorProvider(provider)) && Type.isFalse(Reflect.hasMetadata(MetadataKey.ParameterMetadatas, provider.target))) {
                throw new Error(`Class dependencies must use an 'Injectable' decorator. class: '${provider.target.name}'`);
            }

            if (provider.multiple) {
                const providers: Provider | Array<Provider> = map.get(provider.identifier) || [];

                if (Type.isArray(providers)) {
                    map.set(provider.identifier, providers.concat(provider));
                }
                else {
                    throw new Error(`One of two or more providers with the same identifier does not use multiple injections. identifier: '${Identifier.stringify(provider.identifier)}'`);
                }
            }
            else {
                if (map.has(provider.identifier)) {
                    throw new Error(`One of two or more providers with the same identifier does not use multiple injections. identifier: '${Identifier.stringify(provider.identifier)}'`);
                }

                map.set(provider.identifier, provider);
            }
        }

        return map;
    }
}
