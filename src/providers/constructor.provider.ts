import { Newable } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#constructor-provider
 */
export class ConstructorProvider extends ProviderBase implements ConstructorProvider.Data<object> {
    public static isConstructorProvider(value: unknown): value is ConstructorProvider {
        return value instanceof ConstructorProvider && value.type === ProviderType.Constructor;
    }

    public readonly target: Newable<object>;
    public readonly dependencies: ReadonlyArray<Identifier>;

    public constructor(data: ConstructorProvider.Data<object>) {
        super(ProviderType.Constructor, data);

        this.target = data.target;
        this.dependencies = (data.dependencies || []).map((dependency: Identifier | InjectionToken<unknown>): Identifier => InjectionToken.isInjectionToken(dependency) ? dependency.identifier : dependency);
    }
}
export namespace ConstructorProvider {
    export interface Data<Type extends object> extends ProviderBase.Data<Type> {
        readonly target: Newable<object>;
        readonly dependencies?: ReadonlyArray<Identifier | InjectionToken<unknown>>;
    }
}
