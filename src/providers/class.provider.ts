import { Newable } from '@typemon/types';
//
import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#class-provider
 */
export class ClassProvider extends ProviderBase implements ClassProvider.Data<object> {
    public static isClassProvider(value: unknown): value is ClassProvider {
        return value instanceof ClassProvider && value.type === ProviderType.Class;
    }

    public readonly target: Newable<object>;

    public constructor(data: ClassProvider.Data<object>) {
        super(ProviderType.Class, data);

        this.target = data.target;
    }
}
export namespace ClassProvider {
    export interface Data<Type extends object> extends ProviderBase.Data<Type> {
        readonly target: Newable<object>;
    }
}
