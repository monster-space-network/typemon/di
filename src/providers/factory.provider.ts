import { Callable } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#factory-provider
 */
export class FactoryProvider extends ProviderBase implements FactoryProvider.Data<unknown> {
    public static isFactoryProvider(value: unknown): value is FactoryProvider {
        return value instanceof FactoryProvider && value.type === ProviderType.Factory;
    }

    public readonly callback: FactoryProvider.Callback<unknown>;
    public readonly dependencies: ReadonlyArray<Identifier>;

    public constructor(data: FactoryProvider.Data<unknown>) {
        super(ProviderType.Factory, data);

        this.callback = data.callback;
        this.dependencies = (data.dependencies || []).map((dependency: Identifier | InjectionToken<unknown>): Identifier => InjectionToken.isInjectionToken(dependency) ? dependency.identifier : dependency);
    }
}
export namespace FactoryProvider {
    export type Callback<Type> = Callable<Array<any>, Type>;

    export interface Data<Type> extends ProviderBase.Data<Type> {
        readonly callback: Callback<Type>;
        readonly dependencies?: ReadonlyArray<Identifier | InjectionToken<unknown>>;
    }
}
