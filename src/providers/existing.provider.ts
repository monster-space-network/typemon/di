import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#existing-provider
 */
export class ExistingProvider extends ProviderBase implements ExistingProvider.Data<object> {
    public static isExistingProvider(value: unknown): value is ExistingProvider {
        return value instanceof ExistingProvider && value.type === ProviderType.Existing;
    }

    public readonly target: Identifier | InjectionToken<unknown>;

    public constructor(data: ExistingProvider.Data<object>) {
        super(ProviderType.Existing, data);

        this.target = data.target;
    }
}
export namespace ExistingProvider {
    export interface Data<Type> extends ProviderBase.Data<Type> {
        readonly target: Identifier | InjectionToken<Type>;
    }
}
