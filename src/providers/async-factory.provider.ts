import { Callable } from '@typemon/types';
//
import { Identifier } from '../identifier';
import { InjectionToken } from '../injection-token';
import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#async-factory-provider
 */
export class AsyncFactoryProvider extends ProviderBase implements AsyncFactoryProvider.Data<unknown> {
    public static isAsyncFactoryProvider(value: unknown): value is AsyncFactoryProvider {
        return value instanceof AsyncFactoryProvider && value.type === ProviderType.AsyncFactory;
    }

    public readonly callback: AsyncFactoryProvider.Callback<unknown>;
    public readonly dependencies: ReadonlyArray<Identifier>;

    public constructor(data: AsyncFactoryProvider.Data<unknown>) {
        super(ProviderType.AsyncFactory, data);

        this.callback = data.callback;
        this.dependencies = (data.dependencies || []).map((dependency: Identifier | InjectionToken<unknown>): Identifier => InjectionToken.isInjectionToken(dependency) ? dependency.identifier : dependency);
    }
}
export namespace AsyncFactoryProvider {
    export type Callback<Type> = Callable<Array<any>, Promise<Type>>;

    export interface Data<Type> extends ProviderBase.Data<Type> {
        readonly callback: Callback<Type>;
        readonly dependencies?: ReadonlyArray<Identifier | InjectionToken<unknown>>;
    }
}
