import { ProviderBase } from './provider-base';
import { ProviderType } from './provider-type';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/providers#value-provider
 */
export class ValueProvider extends ProviderBase implements ValueProvider.Data<unknown> {
    public static isValueProvider(value: unknown): value is ValueProvider {
        return value instanceof ValueProvider && value.type === ProviderType.Value;
    }

    public readonly value: unknown;

    public constructor(data: ValueProvider.Data<unknown>) {
        super(ProviderType.Value, data);

        this.value = data.value;
    }
}
export namespace ValueProvider {
    export interface Data<Type> extends ProviderBase.Data<Type> {
        readonly value: Type;
    }
}
