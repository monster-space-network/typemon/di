import { Newable, Type } from '@typemon/types';
//
//
//
export type Identifier = symbol | Newable<object>;
export namespace Identifier {
    export function stringify(identifier: Identifier | Identifiable): string {
        return Type.isSymbol(identifier)
            ? identifier.toString()
            : Type.isFunction(identifier)
                ? identifier.name
                : 'identifier' in identifier
                    ? identifier.identifier.toString()
                    : `${identifier}`;
    }
}

export interface Identifiable {
    readonly identifier: Identifier;
}
