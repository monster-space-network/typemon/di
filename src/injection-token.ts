import { Type } from '@typemon/types';
//
import { Identifiable } from './identifier';
import { EmptyStringIdentifierError } from './errors';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/injection-token
 */
export class InjectionToken<Type> implements Identifiable {
    public static isInjectionToken<Type>(value: unknown): value is InjectionToken<Type> {
        return value instanceof InjectionToken;
    }

    public readonly identifier: symbol;

    public constructor(identifier: string | symbol) {
        this.identifier = Type.isSymbol(identifier)
            ? identifier
            : Type.isEmptyString(identifier)
                ? EmptyStringIdentifierError.throw()
                : Symbol.for(identifier);
    }

    public toString(): string {
        return this.identifier.toString();
    }
}
