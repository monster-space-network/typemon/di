import { MetadataKey } from '../metadata-key';
import { ParameterMetadata } from '../parameter-metadata';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/skip-self
 */
export function SkipSelf(): ParameterDecorator {
    return (target: Object, propertyKey: unknown, index: number): void => {
        const parameterMetadatas: Array<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target) || [];
        const parameterMetadata: ParameterMetadata = parameterMetadatas[index] || new ParameterMetadata(index);

        if (parameterMetadata.flag & ParameterMetadata.Flag.SkipSelf) {
            throw new Error(`Do not duplicate 'SkipSelf' decorators.`);
        }

        parameterMetadata.flag |= ParameterMetadata.Flag.SkipSelf;
        parameterMetadatas[index] = parameterMetadata;

        Reflect.defineMetadata(MetadataKey.ParameterMetadatas, parameterMetadatas, target);
    };
}
