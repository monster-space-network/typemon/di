import { Type } from '@typemon/types';
//
import { Identifier } from '../../identifier';
import { InjectionToken } from '../../injection-token';
import { EmptyStringIdentifierError } from '../../errors';
import { MetadataKey } from '../metadata-key';
import { ParameterMetadata } from '../parameter-metadata';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/inject
 */
export function Inject(identifier: Identifier | InjectionToken<any>): ParameterDecorator {
    if (Type.isSymbol(identifier) && identifier.toString().length === 8) {
        return EmptyStringIdentifierError.throw();
    }

    return (target: Object, propertyKey: unknown, index: number): void => {
        const parameterMetadatas: Array<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target) || [];
        const parameterMetadata: ParameterMetadata = parameterMetadatas[index] || new ParameterMetadata(index);

        if (Type.isNotNull(parameterMetadata.identifier)) {
            throw new Error(`Do not duplicate 'Inject' decorators.`);
        }

        parameterMetadata.identifier = InjectionToken.isInjectionToken(identifier) ? identifier.identifier : identifier;
        parameterMetadatas[index] = parameterMetadata;

        Reflect.defineMetadata(MetadataKey.ParameterMetadatas, parameterMetadatas, target);
    };
}
