import { Identifier } from '../../identifier';
import { MetadataKey } from '../metadata-key';
import { ParameterMetadata } from '../parameter-metadata';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/injectable
 */
export function Injectable(): ClassDecorator {
    return (target: Function): void => {
        if (Reflect.hasMetadata(MetadataKey.ParameterTypes, target)) {
            throw new Error(`Do not duplicate 'Injectable' decorators.`);
        }

        const types: ReadonlyArray<unknown> = Reflect.getMetadata(MetadataKey.DesignParameterTypes, target) || [];
        const parameterMetadatas: Array<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target) || [];

        for (const [index, type] of types.entries()) {
            const parameterMetadata: ParameterMetadata = parameterMetadatas[index] || new ParameterMetadata(index);

            parameterMetadata.type = type;
            parameterMetadata.identifier = parameterMetadata.identifier || type as Identifier;
            parameterMetadatas[index] = parameterMetadata;
        }

        Reflect.defineMetadata(MetadataKey.ParameterTypes, types, target);
        Reflect.defineMetadata(MetadataKey.ParameterMetadatas, parameterMetadatas, target);
    };
}
