import { MetadataKey } from '../metadata-key';
import { ParameterMetadata } from '../parameter-metadata';
//
//
//
/**
 * @see https://gitlab.com/monster-space-network/typemon/di/wikis/self
 */
export function Self(): ParameterDecorator {
    return (target: Object, propertyKey: unknown, index: number): void => {
        const parameterMetadatas: Array<ParameterMetadata> = Reflect.getMetadata(MetadataKey.ParameterMetadatas, target) || [];
        const parameterMetadata: ParameterMetadata = parameterMetadatas[index] || new ParameterMetadata(index);

        if (parameterMetadata.flag & ParameterMetadata.Flag.Self) {
            throw new Error(`Do not duplicate 'Self' decorators.`);
        }

        parameterMetadata.flag |= ParameterMetadata.Flag.Self;
        parameterMetadatas[index] = parameterMetadata;

        Reflect.defineMetadata(MetadataKey.ParameterMetadatas, parameterMetadatas, target);
    };
}
