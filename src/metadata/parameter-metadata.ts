import { Identifier } from '../identifier';
//
//
//
export class ParameterMetadata {
    public static isParameterMetadata(value: unknown): value is ParameterMetadata {
        return value instanceof ParameterMetadata;
    }

    public type: unknown;
    public identifier: Identifier | null;
    public flag: ParameterMetadata.Flag;

    public constructor(
        public readonly index: number
    ) {
        this.type = null;
        this.identifier = null;
        this.flag = ParameterMetadata.Flag.Default;
    }
}
export namespace ParameterMetadata {
    export enum Flag {
        Default = 1 << 0,
        Optional = 1 << 1,
        Self = 1 << 2,
        SkipSelf = 1 << 3
    }
}
