//
//
//
export enum MetadataKey {
    DesignType = 'design:type',
    DesignParameterTypes = 'design:paramtypes',
    DesignReturnType = 'design:returntype',

    ParameterTypes = 'di:parameter-types',
    ParameterMetadatas = 'di:parameter-metadatas'
}
